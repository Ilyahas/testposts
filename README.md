# Posts API

Docs: http://localhost:3000/
or https://testposts.herokuapp.com/

## AUTH
### POST

    /singup:
    body:
        - email
        - password

    /signin
    body:
        - email
        - password
    return:
        - token: String


## Posts
for posts end-points you need to provide auth token
### GET

    /page/:page
    return:
        - array of posts on page

    /:postId
    return:
        - post by id

### POST
    access via auth token
    /
    body:
        - body: String
    return:
        - post id

### PATCH

    access via auth token
    /:postId
    body (Array of objects witch contains property name and its value to update):
        [
            {
                "name": "String"
            }
        ]

### DELETE
    access via auth token
    /:postId
    
    
    


Postman:
    
    POST /auth/signup
    {
      "username": "user1",
      "password": "qwerty123"
    }
    
    POST /auth/signin
    {
      "username": "user1",
      "password": "qwerty123"
    }
    
    DELETE /posts/{postId}
    require auth token (firstly you need to signin and get token)
    
    POST /posts/
    require auth token
    
    PATCH /posts/{postId}
    require auth token
    
    